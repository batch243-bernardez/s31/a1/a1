/*- Import the http module using the required directive.
- Create a variable port and assign it with the value of 3000.
- Create a server using the createServer method that will listen in
to the port provided above.
- Console log in the terminal a message when the server is
successfully running.
- Create a condition that when the login route is accessed, it will
print a message to the user that they are in the login page.*/

const http = require("http");

let url = require('url');

const port = 3000;

const server = http.createServer((request,response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('You are in the Login Page!');
	}

	else{
		response.writeHead(400, {'Content-Type': 'text/plain'});
		response.end(`I'm sorry, the page you are looking for cannot be found.`)
	}
});

server.listen(port);
console.log(`Server now accessible at localhost: ${port}`);
